# Change Log
All notable changes to this project will be documented in this file.

> Before releasing 1.0.0 API breaking changes will increase `minor` version. Everything else is `patch`.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.2.4] - 2023-07-31
* **Changed** Updated dependencies to latest version.

* **Added** SVGO optimization for converting path transforms.

## [0.2.3] - 2022-06-21
* **Changed** Updated dependencies to latest version.

## [0.2.2] - 2021-05-18
* **Added** **Experimental** Type definition output. The result is similar to `@fortawesome/fortawesome-common-types`. However the `IconPrefix` and `IconName` types have been loosened up to be simply `strings`. This sacrifices name and prefix code completion. On the other hand the strict interface of the `@fortawesome/fontawesome-svg-core` `library.add` and `findIconDefinition` has been extended to allow `string`s for these values putting the type checker at ease.

## [0.2.1] - 2021-04-01
* **Changed** No longer offer a default export. It's exported as `awesomifySvgs` instead.
* **Fixed** Updated dependencies to latest audited versions.

## [0.2.0] - 2021-04-01
Never published due to a `package-lock.json` issue.

## [0.1.11] - 2020-06-22
* **Changed** `SVGO` config has been set to `full` mode. This prevents accidental config changes across SVGO versions that may alter and subtly break icon conversion.

## [0.1.10] - 2020-06-18
* **Fixed** Naive path scaling using `svgpath` instead.

## [0.1.9] - 2020-06-17
* **Changed** Instead of opting out with `--no-scale` changed behavior to opt-in with `--auto-scale` to prevent breaking some SVG collections.

## [0.1.8] - 2020-06-17
* **Fixed** Using deprecated `rollup-plugin-virtual`, swapping in `@rollup/plugin-virtual`.

## [0.1.7] - 2020-06-17
* **Added** Auto scaling `viewBox` and `path` coordinates to match `Font Awesome`'s 512 unit expectation. Use `--no-scale` to opt out.
* **Changed** Updated dependencies to latest audited versions.

## [0.1.6] - 2019-10-03
* **Fixed** Jest being a prod dependency.
* **Changed** Updated dependencies to latest version.

## [0.1.5] - 2019-09-09
* **Fixed** CJS output now uses `var` instead of `const` to prevent `IE11` or `UglifyJs` from blowing up.

## [0.1.4] - 2019-08-28
* **Changed** Updated dependencies to latest version.

* **Fixed** Fix regression where `fallbackName` property got missing from ESM formatted index.

## [0.1.3] - 2019-07-05
* **Fixed** Latest formatted icons should be tree-shakeable now even if fallbacks are generated.

## [0.1.2] - 2019-05-25
* **Changed** Replaced deprecated `svgson-next` with `svgson`

## [0.1.1] - 2019-05-25
* **Fixed** Regression bug that included `-all` variants of icons in the `Font Awesome 5.0` (`v5-cjs`) formatted index output.
* **Changed** Updated to latest `rollup` version

## [0.1.0] - 2019-05-10
* **Breaking change** changed prefix generation for deeply embedded icon directories. `/subdir/deep/` with `--prefix the` option used to become `the-deep-subdir` from now on it will be `the-subdir-deep`. As this good intentioned "feature" was mostly the source of confusion

## [0.0.7] - 2018-06-20
* **Added** Better error reporting when parsing SVG to include `cause` and `args` in the thrown `Error`.
* **Fixed** Failing to parse SVG dimensions when `viewBox` is missing, but `width` and `height` is present.

## [0.0.6] - 2018-06-20
* **Added** `-D --no-deep` and `deep` options to **CLI** and **API** respectively to allow shallow converting icons

## [0.0.5] - 2018-06-06
* **Added** `fallbackName` metadata for generated icons with fallback

## [0.0.4] - 2018-05-24
* **Fixed** Not being able to convert multiple paths
* **Fixed** Can't collapse groups with `fill` attribute
* **Fixed** Can't convert icons with shapes

## [0.0.3] - 2018-05-12
* **Fixed** Only work from source when grouping icons

## [0.0.2] - 2018-05-11
* **Hotfix** Accidental self importing in grouped icons. 
* **Hotfix** Some icons missing from group imports with 250ms timeout. Proper solution will follow.

## [0.0.1] - 2018-05-11
* **Experimental release. See README for details**

