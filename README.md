# awesomify-svgs [![pipeline status](https://gitlab.com/riovir/awesomify-svgs/badges/master/pipeline.svg)](https://gitlab.com/riovir/awesomify-svgs/commits/master)[![coverage report](https://gitlab.com/riovir/awesomify-svgs/badges/master/coverage.svg)](https://gitlab.com/riovir/awesomify-svgs/commits/master)

> Convert a directory with SVGs to a Font Awesome 5 supported js data object format. _Warning, this package is currently experimental. Use it at your own risk!_

# CLI

For minimal hassle use cases.

```bash
awesomify-svgs --help

  Usage: cli [options] <source directory> [target directory]

  Converts SVGs in a directory to Font Awesome 5 supported js data object format.
Examples:

Convert SVGs from /example-icons to /dist without any grouping:
> awesomify-svgs example-icons

Some designers prefer to create the same icon multiple times with different detail levels.
Eg. user-16.svg is ideal for 16px size while user-32 has more details. Setting --has-variant with
the appropriate separator reults in [icon]-all.js files to be generated with imports to all variant:
> awesomify-svgs --has-variant example-icons

Sometimes certain SVG variants might be missing. Awesomify can be set to generate fallbacks with the
--ensured-variants option. Eg. To make sure [icon]-16, [icon]-32 and [icon]-48 are always ready to import:
> awesomify-svgs --has-variant --ensured-variants "16, 32, 48" example-icons


  Options:

    -V, --version                                    output the version number
    -D, --no-deep                                    ignore sub-directories
    -f, --format [name]                              Font Awesome format version: accepts "latest", "v5-cjs", "v5-esm" (default: "latest")
    -n, --namespace [name]                           Prepending the prefix, used as a namespace for UMD bundled index.js files. (default: "Awesomified")
    -a, --auto-scale                                 Enables auto-scaling viewBox and path coordinates to 512 units
    -c, --clean                                      cleans target directory first
    -p, --prefix [name]                              sets icon prefix (default: "the")
    -v, --has-variant [separator]                    use naming convention of [name][separator][variant] (default: -)
    -e, --ensured-variants [variant 1]..[variant n]  comma separated icon variants to always generate (default: [])
    -h, --help                                       output usage information
```

# Node API

Use to gain better control over the behavior.

```TypeScript
/** Converts SVG icons in a dir to a FontAwesome compliant icon pack. */
export function awesomifySvgs(options?: AwesomifyOptions): Promise<Array<Promise<void>>>;

type AwesomifyOptions = {
  /** Relative or absolute path to SVGs to convert. Defaults to "icons". */
  source?: string,
  /** Relative or absolute path to target directory. Defaults to "dist". */
  target?: string,
  /** Output FontAwesome format to use. Defaults to "latest". */
  format?: FontAwesomeFormat,
  /** Prepending the prefix, used as a namespace for UMD bundled index.js files. Defaults to "Awesomify". */
  namespace?: string,
  /** Opts into auto-scaling viewBox and path coordinates to 512. Defaults to false. */
  autoScale?: boolean,
  /** Deep scan (recursively) the source directory for icons. Defaults to true. */
  deep?: boolean,
  /** Wipe target first. Defaults to false. */
  cleanTarget?: boolean,
  /** Set the prefix property on the icon entries. Any subdirectories will be appended to this inside the source. Defaults to "the". */
  prefix?: string,
  /** ADVANCED: Group icon variants together. Eg. to recognize user-small and user-large both as user, chop off the last bit. */
  toBaseName?: (relativeName: string) => string,
  /** ADVANCED: Generate fallbacks for missing icons. Eg. to ensure user-small, user-medium, and user-large to exist, return them in an array */
  ensureVariants?: (baseName: string) => Array<string>
};

type FontAwesomeFormat =
  /** Latest supported FontAwesome format */
  'latest' |
  /** Adds an tree-shakeable ESM based format (index.es.js) introduced in FontAwesome 5.1 */
  'v5-esm' |
  /** CommonJS based format in FontAwesome 5.0 */
  'v5-cjs';
```
