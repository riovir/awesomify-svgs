#!/usr/bin/env node

const program = require('commander');
const { always, includes, dropLast, identity, is, join, last, map, pipe, split, trim, when } = require('ramda');
const { version } = require('./package');
const { awesomifySvgs } = require('./lib');

program
  .version(version)
  .usage('[options] <source directory> [target directory]')
  .description(`Converts SVGs in a directory to Font Awesome 5 supported js data object format.
Examples:

Convert SVGs from /example-icons to /dist without any grouping:
> awesomify-svgs example-icons

Some designers prefer to create the same icon multiple times with different detail levels.
Eg. user-16.svg is ideal for 16px size while user-32 has more details. Setting --has-variant with
the appropriate separator results in [icon]-all.js files to be generated with imports to all variant:
> awesomify-svgs --has-variant example-icons

Sometimes certain SVG variants might be missing. Awesomify can be set to generate fallbacks with the
--ensured-variants option. Eg. To make sure [icon]-16, [icon]-32 and [icon]-48 are always ready to import:
> awesomify-svgs --has-variant --ensured-variants "16, 32, 48" example-icons`)
  .arguments('[source] [target]')
  .option('-D, --no-deep', "don't scan sub-directories")
  .option('-f, --format [name]', 'Font Awesome format version: accepts "latest", "v5-cjs", "v5-esm"', identity, 'latest')
  .option('-n, --namespace [name]', 'Prepending the prefix, used as a namespace for UMD bundled index.js files.', identity, 'Awesomified')
  .option('-a, --auto-scale', 'enables auto-scaling viewBox and path coordinates to 512 units')
  .option('-c, --clean', 'cleans target directory first')
  .option('-p, --prefix [name]', 'set icon prefix', identity, 'the')
  .option(
    '-v, --has-variant [separator]',
    'use naming convention of [name][separator][variant] (default: -)')
  .option(
    '-e, --ensured-variants [variant 1]..[variant n]',
    'comma separated icon variants to always generate',
    parseList, [])
  .action(runAwesomify)
  .parse(process.argv);

function parseList(text) {
  return pipe(split(','), map(trim))(text);
}

/* eslint-disable no-console */
function runAwesomify(source, target, { deep, format, namespace, autoScale, clean, prefix, hasVariant, ensuredVariants }) {
  const separator = when(is(Boolean), always('-'), hasVariant);
  const toBaseName = hasVariant ? withoutVariant(separator) : identity;
  const ensureVariants = ensuredVariants.length ?
    baseName => ensuredVariants.map(variant => `${baseName}${separator}${variant}`) :
    always([]);
  return awesomifySvgs({ source, target, deep, format, namespace, autoScale, cleanTarget: clean, prefix, toBaseName, ensureVariants })
    .catch(err => { console.log(err); });
}

function withoutVariant(separator) {
  const dropVariant = pipe(split(separator), dropLast(1), join(separator));
  const fileHasVariant = pipe(split('/'), last, includes(separator));
  return when(fileHasVariant, dropVariant);
}
