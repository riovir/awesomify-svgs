module.exports.SvgFile = function SvgFile({
  name,
  relativeParent = '',
  relativeName = name,
  size = 512,
  width = size,
  height = size,
  viewBox = true,
  content = svgContent({ width, height, name, viewBox }),
  read = async () => content
}) {
  return { name, relativeParent, relativeName, content, read };
};

function svgContent({ width, height, name, viewBox }) {
  const viewBoxAttr = viewBox ? `viewBox="0 0 ${width} ${height}"` : '';
  return `<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="${width}" height="${height}" ${viewBoxAttr}>
    <title>${name}</title>
    <path d="M${width} ${height}c-8.837 0-16 7.163-16 16s7.163 16 16 16 16-7.163 16-16-7.163-16-16-16z"></path>
  </svg>
  `;
}
