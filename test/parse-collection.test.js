const { pick, prop, propEq } = require('ramda');
const { parseCollection } = require('../lib/parse-collection');
const { SvgFile } = require('./svg-file');

const parseSimple = parseCollection({ prefix: 'smp', namespace: 'Simple' });
const parseFancy = parseCollection({
  prefix: 'fcy',
  namespace: 'Fancy',
  toBaseName: name => name.split('-')[0],
  ensureVariants: name => [`${name}-low`, `${name}-hd`]
});

beforeEach(() => {
  console.warn = jest.fn();
});
afterEach(() => {
  console.warn.mockReset();
});

test('returns empty array for blank input', async () => {
  expect(await parseSimple()).toHaveLength(0);
  expect(await parseSimple()).toHaveLength(0);
  expect(await parseFancy([])).toHaveLength(0);
  expect(await parseFancy([])).toHaveLength(0);
});

test('finds suitable fallback when ensureVariants option is used', async () => {
  const results = await parseFancy([
    SvgFile({ name: 'one-low' })
  ]);
  const existingIcon = results.find(matching({ name: 'one-low' }));
  const { fallback } = results.find(matching({ name: 'one-hd' }));
  expect(fallback).toEqual(existingIcon);
});

test('parses out data required by writing Font Awesome definitions', async () => {
  const initialFile = SvgFile({ name: 'test', width: 42, height: 24 });
  const icons = await parseSimple([initialFile]);
  const converted = icons.find(matching(initialFile));
  expect(converted).toEqual(expect.objectContaining({
    ...pick(['relativeParent', 'relativeName', 'name'], initialFile),
    prefix: 'smp',
    type: 'existing',
    data: expect.objectContaining({
      width: 42,
      height: 24,
      iconName: 'test',
      prefix: 'smp',
      svgPathData: expect.any(String),
      unicode: expect.any(String),
    })
  }));
});

test('autoScale option opts into auto scaling viewBox', async () => {
  const initialFile = SvgFile({ name: 'test', width: 42, height: 24 });
  const parse = parseCollection({ autoScale: true, prefix: 'smp', namespace: 'Simple' });
  const icons = await parse([initialFile]);
  const converted = icons.find(matching(initialFile));
  expect(converted).toEqual(expect.objectContaining({
    data: expect.objectContaining({ width: 512 })
  }));
});

test('adds index containing every other icon', async () => {
  const results = await parseSimple([
    SvgFile({ name: 'one' }),
    SvgFile({ name: 'two' }),
    SvgFile({ name: 'three' }),
  ]);
  const { allIcons } = results.find(matching({ name: 'index' }));
  expect(allIcons.map(prop('name'))).toEqual(expect.arrayContaining(['one', 'two', 'three']));
});

test('warns on invalid dimensions', async () => {

  await parseSimple([
    SvgFile({ name: 'too-small', size: 512 - 1, autoScale: false }),
  ]);
  expect(console.warn).toHaveBeenCalled();
});

test('skips warning on invalid dimensions when auto-scale is on', async () => {
  const parse = parseCollection({ autoScale: true });
  await parse([
    SvgFile({ name: 'too-small', size: 512 - 1 }),
  ]);
  expect(console.warn).not.toHaveBeenCalled();
});

function matching({ name }) {
  return propEq(name, 'name');
}
