const { EXISTING_ICON, EXISTING_ICONS_INDEX, INDEX_FILE, MISSING_ICON }  = require('./test-outputs');
const { v5esm }  = require('../../lib/formatters');

/**
 * Welp, at least the bundling shouldn't blow up outright...
 * TODO: find way to test the generated ESM files maybe via JSDOM once it supports
 * <script type="module"></script>
 */

test('existingIconOutput bundling succeeds', () => {
  return v5esm.existingIconOutput(EXISTING_ICON);
});

test('missingIconOutput bundling succeeds', () => {
  return v5esm.missingIconOutput(MISSING_ICON);
});

test('indexFileOutput bundling succeeds', async () => {
  await v5esm.indexFileOutput(EXISTING_ICONS_INDEX);
  await v5esm.indexFileOutput({ ...EXISTING_ICONS_INDEX, relativeParent: 'relative' });
  await v5esm.indexFileOutput(INDEX_FILE);
});
