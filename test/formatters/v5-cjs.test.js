const { EXISTING_ICON, EXISTING_ICONS_INDEX, INDEX_FILE, MISSING_ICON }  = require('./test-outputs');
const { v5cjs }  = require('../../lib/formatters');

test('existingIconOutput has Font Awesome 5 format', () => {
  const { content } = v5cjs.existingIconOutput(EXISTING_ICON);
  expectFontAwesomeFormat(evalExport({ content }));
});

test('missingIconOutput has Font Awesome 5 format', () => {
  const { content } = v5cjs.missingIconOutput(MISSING_ICON);
  const requiredMock = { prefix: 'p', iconName: 'x', icon: [1, 2, [], 'a', 'b'] };
  expectFontAwesomeFormat(evalExport({ content, requiredMock }));
});

test('indexFileOutput has array of Font Awesome 5 format', () => {
  const { content } = v5cjs.indexFileOutput(EXISTING_ICONS_INDEX);
  const requiredMock = { prefix: 'p', iconName: 'x', icon: [1, 2, [], 'a', 'b'] };
  evalExport({ content, requiredMock }).forEach(expectFontAwesomeFormat);
});

test('indexFileOutput has array without index typed icons', () => {
  const { content } = v5cjs.indexFileOutput(INDEX_FILE);
  expect(content).not.toEqual(expect.stringContaining(EXISTING_ICONS_INDEX.name));
});

test('ensures relativeName ends with .js', () => {
  checkHasJsExtensions(v5cjs.existingIconOutput(EXISTING_ICON));
  checkHasJsExtensions(v5cjs.existingIconOutput({ ...EXISTING_ICON, relativeName: 'test.js' }));

  checkHasJsExtensions(v5cjs.missingIconOutput(MISSING_ICON));
  checkHasJsExtensions(v5cjs.missingIconOutput({ ...MISSING_ICON, relativeName: 'test.js' }));

  checkHasJsExtensions(v5cjs.indexFileOutput(INDEX_FILE));
  checkHasJsExtensions(v5cjs.indexFileOutput({ ...INDEX_FILE, relativeName: 'index.js' }));
});

function checkHasJsExtensions({ relativeName }) {
  expect(relativeName).toEqual(expect.stringMatching(/\.js$/));
  expect(relativeName).not.toEqual(expect.stringMatching(/\.js\.js$/));
}

function expectFontAwesomeFormat(object) {
  expect(object).toEqual(expect.objectContaining({
    prefix: expect.any(String),
    iconName: expect.any(String),
    icon: [
      expect.any(Number),
      expect.any(Number),
      expect.any(Array),
      expect.any(String),
      expect.any(String)
    ]
  }));
}

function evalExport({ content, requiredMock = {} }) {
  const fallbackJson = JSON.stringify(requiredMock);
  return eval(`
    function scope(module, require) {
      ${content}
      return module.exports;
    }
    scope({ exports: {} }, () => JSON.parse('${fallbackJson}'));
  `);
}
