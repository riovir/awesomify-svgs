const { ExistingIcon, MissingIcon, IndexFile }  = require('../../lib/outputs');

const EXISTING_ICON = ExistingIcon({
  relativeParent: '',
  relativeName: 'test',
  name: 'test',
  data: {
    prefix: 'test',
    iconName: 'icon',
    width: 32,
    height: 48,
    ligatures: [],
    unicode: 'f000',
    svgPathData: 'M16',
  }
});
const MISSING_ICON = MissingIcon({
  relativeName: 'missing',
  name: 'missing',
  fallback: EXISTING_ICON
});

const EXISTING_ICONS_INDEX = IndexFile({
  relativeName: 'test-all',
  name: 'test-all',
  allIcons: [EXISTING_ICON]
});

const INDEX_FILE = IndexFile({
  namespace: 'TestPack',
  allIcons: [EXISTING_ICON, MISSING_ICON, EXISTING_ICONS_INDEX]
});

module.exports.EXISTING_ICON = EXISTING_ICON;
module.exports.MISSING_ICON = MISSING_ICON;
module.exports.EXISTING_ICONS_INDEX = EXISTING_ICONS_INDEX;
module.exports.INDEX_FILE = INDEX_FILE;
