const { outputType } = require('./outputs');
const { generateOutput } = require('./generate-output');

const { existing, missing, index } = outputType;

const formatter = MockFormatter();

test('promises formatted outputs of existing icon', () => {
  return expectFormatting({
    asset: { name: 'test', type: existing },
    resultEquals: `ex-${existing}-test`
  });
});

test('promises formatted outputs of missing icon', () => {
  return expectFormatting({
    asset: { name: 'test', type: missing },
    resultEquals: `ms-${missing}-test`
  });
});

test('promises formatted outputs of index files', () => {
  return expectFormatting({
    asset: { name: 'test', type: index },
    resultEquals: `in-${index}-test`
  });
});

test('flattens out any returned array from formatter', async () => {
  const generate = generateOutput({ formatter: MockFormatter({ multipleOutputs: true }) });
  const [ex1, ex2, ms1, ms2, in1, in2, ...rest] = await generate([
    { name: 'e', type: existing },
    { name: 'm', type: missing },
    { name: 'i', type: index },
  ]);
  expect(rest).toHaveLength(0);
  expect(ex1.result).toEqual(`ex-${existing}-e-1`);
  expect(ex2.result).toEqual(`ex-${existing}-e-2`);
  expect(ms1.result).toEqual(`ms-${missing}-m-1`);
  expect(ms2.result).toEqual(`ms-${missing}-m-2`);
  expect(in1.result).toEqual(`in-${index}-i-1`);
  expect(in2.result).toEqual(`in-${index}-i-2`);
});

test('skips unknown types', async () => {
  const generate = generateOutput({ formatter });
  const [first, ...rest] = await generate([
    { name: 'test', type: existing },
    { name: 'unknown', type: 'unknown' }
  ]);
  expect(rest).toHaveLength(0);
  expect(first.result).toEqual(`ex-${existing}-test`);
});

async function expectFormatting({ asset, resultEquals }) {
  const generate = generateOutput({ formatter });
  const [first, ...rest] = await generate([asset]);
  expect(rest).toHaveLength(0);
  expect(first.result).toEqual(resultEquals);
}

function MockFormatter({ multipleOutputs = false } = {}) {
  const withAlias = alias => multipleOutputs ?
    ({ name, type }) => [
      ({ result: `${alias}-${type}-${name}-1` }),
      ({ result: `${alias}-${type}-${name}-2` })
    ] :
    ({ name, type }) => ({ result: `${alias}-${type}-${name}` });
  return {
    existingIconOutput: withAlias('ex'),
    missingIconOutput: withAlias('ms'),
    indexFileOutput: withAlias('in')
  };
}

