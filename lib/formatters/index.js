module.exports = {
  v5cjs: require('./v5-cjs'),
  v5esm: require('./v5-esm')
};
