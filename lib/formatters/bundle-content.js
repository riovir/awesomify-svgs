const { rollup } = require('rollup');
const virtual = require('@rollup/plugin-virtual');

module.exports.bundleContent = async function bundleContent({
  content,
  importedContent = {},
  format,
  namespace,
}) {
  const { generate } = await rollup({
    input: 'entry',
    plugins: [
      virtual({
        'entry': content,
        ...importedContent
      }),
    ]
  });
  const { output } = await generate({
    name: namespace,
    file: 'bundle.umd.js',
    format
  });
  const [firstChunk] = output;
  return firstChunk.code;
};
