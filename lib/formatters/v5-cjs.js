const { outputType } = require('../outputs');
const { JsOutput } = require('./js-output');

module.exports.existingIconOutput = function existingIconOutput({ relativeName, data }) {
  const { prefix, iconName, width, height, unicode, svgPathData } = data;
  const content =
    `module.exports = { prefix: '${prefix}', iconName: '${iconName}', icon: [${width}, ${height}, [], '${unicode}', '${svgPathData}'] };\n`;
  return JsOutput({ relativeName, content });
};

module.exports.missingIconOutput = function missingIconOutput({ relativeName, name, fallback }) {
  const content =
    `var fallback = require('./${fallback.name}');\n` +
    `module.exports = { prefix: fallback.prefix, iconName: '${name}', icon: fallback.icon, fallbackName: fallback.iconName };\n`;
  return JsOutput({ relativeName, content });
};

module.exports.indexFileOutput = function indexFileOutput({ relativeName, directIcons }) {
  const requires = directIcons
      .filter(({ type }) => type !== outputType.index)
      .map(({ name }) => `  require('./${name}')`).join(',\n');
  const content = `module.exports = [\n${requires}\n];\n`;
  return JsOutput({ relativeName, content });
};
