const { toCamelCase } = require('../utils');

const COMMON_TYPES_FILE = 'custom-common-types';
const FONT_AWESOME_SVG_CORE_FILE = 'custom-fontawesome-svg-core';

module.exports.BASE_OUTPUT = Object.freeze([
  Object.freeze(CommonTypes()),
  Object.freeze(FontAwesomeSvgCoreOutput()),
]);

module.exports.indexTypeDefsOutput = function indexTypeDefsOutput({ relativeName, iconsByPrefix, allIcons }) {
  const definitions = allIcons.map(exportNameOf);
  const packs = Object.keys(iconsByPrefix).map(toCamelCase);
  return CustomTypes({ relativeName, definitions, packs });
};

module.exports.iconTypeDefOutput = function iconTypeDefOutput({ relativeName, prefix, name }) {
  const exportName = exportNameOf({ prefix, name });
  const relativeCommonTypes = relativeRootOf(relativeName).concat(COMMON_TYPES_FILE);
  return { relativeName: `${relativeName}.d.ts`, content:
`import { IconDefinition, IconPrefix, IconName } from '${relativeCommonTypes}';
export const definition: IconDefinition;
export const ${exportName}: IconDefinition;
export const prefix: IconPrefix;
export const iconName: IconName;
export const width: number;
export const height: number;
export const ligatures: string[];
export const unicode: string;
export const svgPathData: string;
` };
};

function CustomTypes({ relativeName, definitions, packs }) {
  const renderContent = (mapper, array) => array.map(mapper).join('\n');
  const root = relativeRootOf(relativeName);
  const relativeCommonTypes = root.concat(COMMON_TYPES_FILE);
  const relativeFontAwesomeSvgCore = root.concat(FONT_AWESOME_SVG_CORE_FILE);

  return { relativeName: `${relativeName}.d.ts`, content:
`import '${relativeFontAwesomeSvgCore}';

${renderContent(def =>
  `export const ${def}: IconDefinition;`, definitions)}
import { IconDefinition, IconPrefix, IconPack } from '${relativeCommonTypes}';
export { IconDefinition, IconPrefix, IconPack, IconLookup, IconName } from '${relativeCommonTypes}';
export const prefix: IconPrefix;
${renderContent(pack =>
  `export const ${pack}: IconPack;`, packs)}
` };
}

function CommonTypes() {
  return { relativeName: `${COMMON_TYPES_FILE}.d.ts`, content:
`export type IconPathData = string | string[];

export interface IconLookup {
  prefix: IconPrefix;
  iconName: IconName;
}

export interface IconDefinition extends IconLookup {
  icon: [
    number, // width
    number, // height
    string[], // ligatures
    string, // unicode
    IconPathData // svgPathData
  ];
}

export interface IconPack {
  [key: string]: IconDefinition;
}

// Allow less strict checking of prefix and name as this is a custom pack
export type IconPrefix = string;
export type IconName = string;
` };
}

function FontAwesomeSvgCoreOutput() {
  return { relativeName: `${FONT_AWESOME_SVG_CORE_FILE}.d.ts`, content:
`import { IconDefinition, IconPack, IconLookup } from './${COMMON_TYPES_FILE}';

declare module '@fortawesome/fontawesome-svg-core' {
	type IconDefinitionOrPack = IconDefinition | IconPack;

	export interface Library {
		add(...definitions: IconDefinitionOrPack[]): void;
	}

	export function findIconDefinition(iconLookup: IconLookup): IconDefinition;
}
` };
}

function relativeRootOf(filePath) {
  const depth = filePath.split('/').length;
  return 1 < depth ? '../'.repeat(depth - 1) : './';
}

function exportNameOf({ prefix, name }) {
  return toCamelCase(`${prefix}-${name}`);
}
