const { mapObjIndexed, pipe, prop, propEq, values } = require('ramda');
const { JsOutput } = require('./js-output');
const { toCamelCase } = require('../utils');
const { outputType } = require('../outputs');
const { bundleContent } = require('./bundle-content');
const { BASE_OUTPUT, indexTypeDefsOutput, iconTypeDefOutput } = require('./type-defs-output');

module.exports.existingIconOutput = async function existingIconOutput({ prefix, name, relativeParent, data }) {
  const content = existingToEsmContent({ prefix, name, data });
  const relativeName = relativeNameOf({ prefix, name, relativeParent });
  const bundledContent = await bundleContent({ format: 'cjs', content });
  return [
    JsOutput({ relativeName, content: bundledContent }),
    iconTypeDefOutput({ relativeName, name, prefix }),
  ];
};

module.exports.missingIconOutput = async function missingIconOutput({ prefix, name, relativeParent, fallback }) {
  const fallbackContent = existingToEsmContent(fallback);
  const content = missingToEsmContent({ prefix, name });
  const relativeName = relativeNameOf({ prefix, name, relativeParent });
  const bundledContent = await bundleContent({ format: 'cjs', content, importedContent: { fallback: fallbackContent } });
  return [
    JsOutput({ relativeName, content: bundledContent }),
    iconTypeDefOutput({ relativeName, name, prefix }),
  ];
};

module.exports.indexFileOutput = async function indexFileOutput({
  prefix, name, relativeParent, relativeName, allIcons, iconsByPrefix, namespace
}) {
  const existingIcons = allIcons.filter(propEq(outputType.existing, 'type'));
  const importedContent = existingIcons.reduce((namedContent, icon) => ({
    ...namedContent,
    [exportNameOf(icon)]: existingToEsmContent(icon)
  }), {});
  const content = indexToEsmContent({ prefix, allIcons, existingIcons, iconsByPrefix });

  const isMainIndex = name === 'index';
  const fileName = isMainIndex ? relativeName : relativeNameOf({ prefix, name, relativeParent });
  const formatProps = isMainIndex ? { format: 'umd', namespace } : { format: 'cjs' };
  const defaultOutput = [
    JsOutput({
      relativeName: fileName,
      content: await bundleContent({ ...formatProps, content, importedContent })
    }),
    indexTypeDefsOutput({ relativeName: fileName, iconsByPrefix, allIcons }),
  ];
  if (!isMainIndex) { return defaultOutput; }
  return [
    ...(relativeName === 'index' ? BASE_OUTPUT : []),
    defaultOutput,
    JsOutput({
      relativeName: `${fileName}.es`,
      content: await bundleContent({ format: 'esm', content, importedContent })
    }),
  ];
};

function existingToEsmContent({ prefix, name, data }) {
  const { prefix: faPrefix, iconName, width, height, unicode, svgPathData } = data;
  return (
`export var prefix = '${faPrefix}';
export var iconName = '${iconName}';
export var width = ${width};
export var height = ${height};
export var ligatures = [];
export var unicode = '${unicode}';
export var svgPathData = '${svgPathData}';

export var definition = {
  prefix: prefix,
  iconName: iconName,
  icon: [
    width,
    height,
    ligatures,
    unicode,
    svgPathData
  ]};

export var ${exportNameOf({ prefix, name })} = definition;
`);
}

function missingToEsmContent({ prefix, name }) {
  return (
`import { iconName as fallbackName, prefix, width, height, unicode, svgPathData } from 'fallback';
export { fallbackName, prefix, width, height, unicode, svgPathData };
export var iconName = '${name}';
export var ligatures = [];

export var definition = {
  prefix: prefix,
  iconName: iconName,
  icon: [
    width,
    height,
    ligatures,
    unicode,
    svgPathData
  ]};

export var ${exportNameOf({ prefix, name })} = definition;
`);
}

function missingToEsmVariableContent({ prefix, name, fallback }) {
  const missingName = exportNameOf({ prefix, name });
  const fallbackName = exportNameOf(fallback);
  return (
`var ${missingName} = {
  prefix: ${fallbackName}.prefix,
  iconName: '${name}',
  icon: ${fallbackName}.icon,
  fallbackName: ${fallbackName}.iconName
};`);
}

function indexToEsmVariableContent({ prefix, name, allIcons }) {
  const exportName = exportNameOf({ prefix, name });
  const iconProps = allIcons.map(exportNameOf)
      .map(name => `  ${name}: ${name}`)
      .join(',\n');
  return (

`var ${exportName} = {
${iconProps}
};

`);
}

function indexToEsmContent({ prefix, allIcons, existingIcons, iconsByPrefix }) {
  const allExportNames = allIcons.map(exportNameOf);
  const existingExportNames = existingIcons.map(exportNameOf);
  const existingImports = existingExportNames
      .map(name => `import { definition as ${name} } from '${name}';\n`)
      .join('');

  const missingIcons = allIcons.filter(propEq(outputType.missing, 'type'));
  const missingVariables = missingIcons
      .map(missingToEsmVariableContent)
      .join('\n');

  const indexFiles = allIcons.filter(propEq(outputType.index, 'type'));
  const indexVariables = indexFiles
      .map(indexToEsmVariableContent)
      .join('\n');

  const iconCaches = pipe(
    mapObjIndexed(toPrefixedIconsCache),
    values
  )(iconsByPrefix);

  const iconCachesContent = iconCaches.map(prop('content')).join('\n');
  const iconCachesExport = iconCaches.map(({ variable, exportedPrefix }) => `${variable} as ${exportedPrefix}`).join(', ');

  return (
`var prefix = '${prefix}';

${existingImports}
${missingVariables}
${indexVariables}
${iconCachesContent}
export { ${iconCachesExport}, prefix, ${[...allExportNames].join(', ')} };
`);
}

function toPrefixedIconsCache(icons, prefix) {
  const exportedPrefix = toCamelCase(prefix);
  const variable = `_${exportedPrefix}IconsCache`;
  const props = icons
      .filter(typeIsEither(outputType.existing, outputType.missing))
      .map(exportNameOf)
      .map((name) => `    ${name}: ${name}`)
      .join(',\n');
  const content = `var ${variable} = {\n${props}\n};\n`;
  return { variable, exportedPrefix, content };
}

function typeIsEither(...types) {
  return ({ type }) => types.includes(type);
}

function exportNameOf({ prefix, name }) {
  return toCamelCase(`${prefix}-${name}`);
}

function relativeNameOf({ prefix, name, relativeParent }) {
  const exportName = exportNameOf({ prefix, name });
  return relativeParent ? `${relativeParent}/${exportName}` : exportName;
}
