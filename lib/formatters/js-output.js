module.exports.JsOutput = function JsOutput({ relativeName, ...rest }) {
  const name = relativeName.endsWith('.js') ? relativeName : `${relativeName}.js`;
  return { relativeName: name, ...rest };
};
