const { find, propEq } = require('ramda');
const { toIconGroups } = require('./to-icon-groups');

const testData = {
  wizard_16: IconFile('wizard-16'),
  wizard_24: IconFile('wizard-24'),
  wizard_32: IconFile('wizard-32'),
  monk_16: IconFile('monk-24'),
  monk_32: IconFile('monk-32'),
  epicWizard: IconFile('epic/wizard')
};
const iconFiles = Object.values(testData);
const withoutSize = name => name.replace(/-\d+$/, '');
const ensure16and32 = name => [`${name}-16`, `${name}-32`];
const sizelessGroups = ['wizard', 'monk', 'epic/wizard'];

describe('by default', () => {
  const groupedFiles = toIconGroups()(iconFiles);

  test('returns array with same size as input', () => {
    return expect(groupedFiles).toHaveLength(iconFiles.length);
  });

  test('leaves expectedIcons empty', () => {
    expect(findExpectedIcons(groupedFiles)).toBeFalsy();
  });

  test('return existingIcons as singleton arrays', () => {
    expect(findNotOneExistingIcon(groupedFiles)).toBeFalsy();
  });
});

describe('when toBaseName is set to return name without a size', () => {
  const groupedFiles = toIconGroups({ toBaseName: withoutSize })(iconFiles);
  const find = findByName(groupedFiles);

  test('returns array with same size icon groups', () => {
    expect(groupedFiles).toHaveLength(sizelessGroups.length);
  });

  test('groups icons with same name', () => {
    expect(find('wizard').existingIcons).toHaveLength(3);
  });

  test('returns icon without a size', () => {
    expect(find('epic/wizard').existingIcons).toHaveLength(1);
  });
});

describe('when ensureVariants is set to return size 16 and 32', () => {
  test('returns all expected icons', () => {
    const groupedFiles = toIconGroups({ toBaseName: withoutSize, ensureVariants: ensure16and32 })(iconFiles);
    const find = findByName(groupedFiles);
    expect(find('epic/wizard').expectedIcons).toEqual(['epic/wizard-16', 'epic/wizard-32']);
  });
});

function findExpectedIcons(groupedFiles) {
  const match = ({ expectedIcons }) => expectedIcons.length > 0;
  return groupedFiles.find(match);
}

function findNotOneExistingIcon(groupedFiles) {
  const match = ({ existingIcons }) => existingIcons.length !== 1;
  return groupedFiles.find(match);
}

function findByName(groups) {
  return name => find(propEq(name, 'name'), groups);
}

function IconFile(relativeName) {
  return { relativeName, someData: `data of ${relativeName}` };
}
