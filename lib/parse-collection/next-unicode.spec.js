const { NextUnicode } = require('./next-unicode');

test('Is factory function returning iterator function', () => {
  const nextUnicode = NextUnicode({ from: '0' });
  expect(nextUnicode()).toBe('0');
  expect(nextUnicode()).toBe('1');
});

test('Counts in hex', () => {
  const nextUnicode = NextUnicode({ from: '9' });
  expect(nextUnicode()).toBe('9');
  expect(nextUnicode()).toBe('a');
  expect(nextUnicode()).toBe('b');
  expect(nextUnicode()).toBe('c');
  expect(nextUnicode()).toBe('d');
  expect(nextUnicode()).toBe('e');
  expect(nextUnicode()).toBe('f');
  expect(nextUnicode()).toBe('10');
});

test('By default starts from private unicode area, f000', () => {
  const nextUnicode = NextUnicode();
  expect(nextUnicode()).toBe('f000');
});
