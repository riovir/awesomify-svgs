const { flatten, map, pipe, prop, propEq } = require('ramda');
const { ExistingIcon, MissingIcon, IndexFile } = require('../outputs');


module.exports.toTypedIcons = function toTypedIcons(groups) {
  return pipe(
    map(toTypedIconGroups),
    map(prop('typedIcons')),
    flatten,
  )(groups);
};

function toTypedIconGroups({ name, existingIcons, missingIcons, ...rest }) {
  const existing = existingIcons
      .map(({ fontAwesomeProps: data, ...rest }) => ({ data, ...rest }))
      .map(ExistingIcon);
  const missing = missingIcons
      .map(addFallbackFrom(existing))
      .map(MissingIcon);
  const allIcons = [...existing, ...missing];
  const index = () => IndexFile({ relativeName: `${name}-all`, allIcons });
  return {
    existingIcons, missingIcons, ...rest,
    typedIcons: allIcons.length > 1 ? [...allIcons, index()] : allIcons
  };
}

function addFallbackFrom(icons) {
  return ({ fallback, ...rest }) => {
    const fallbackIcon = icons.find(propEq(fallback.relativeName, 'relativeName'));
    return { ...rest, fallback: fallbackIcon };
  };
}
