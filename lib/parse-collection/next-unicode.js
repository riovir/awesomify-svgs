module.exports.NextUnicode = function NextUnicode({ from = 'f000' } = {}) {
  let icons = 0;
  return function nextUnicode() {
    const hex = parseInt(from, 16) + icons;
    icons++;
    return hex.toString(16);
  };
};
