const { scaleIcon } = require('./scale-icon');

test('scales dimensions to longerAxis', () => {
  const { scale, icon, expectIcon } = setup({ width: 256, height: 256, longerAxis: 512 });
  expect(scale(icon)).toEqual(expectIcon({ width: 512, height: 512 }));
});

test('scales larger width to longerAxis', () => {
  const { scale, icon, expectIcon } = setup({ width: 256, height: 128, longerAxis: 512 });
  expect(scale(icon)).toEqual(expectIcon({ width: 512, height: 256 }));
});

test('scales larger height to longerAxis', () => {
  const { scale, icon, expectIcon } = setup({ width: 128, height: 256, longerAxis: 512 });
  expect(scale(icon)).toEqual(expectIcon({ width: 256, height: 512 }));
});

test('scales up coordinates', () => {
  const { scale, icon, expectIcon } = setup({ svgPathData: 'M1-1L-.5 0Z', width: 2, longerAxis: 4 });
  expect(scale(icon)).toEqual(expectIcon({ svgPathData: 'M2-2L-1 0Z' }));
});

test('cuts off trailing zeroes', () => {
  const { scale, icon, expectIcon } = setup({ svgPathData: 'M1-1.0000000', width: 2, longerAxis: 4 });
  expect(scale(icon)).toEqual(expectIcon({ svgPathData: 'M2-2' }));
});

function setup({ width, height = width, svgPathData = '', ...args }) {
  const fontAwesomeProps = { width, height, svgPathData };
  const icon = { fontAwesomeProps };

  const scale = scaleIcon(args);
  const expectIcon = props => expect.objectContaining({
    fontAwesomeProps: expect.objectContaining(props),
  });
  return { scale, icon, expectIcon };
}
