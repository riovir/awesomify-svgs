const svgPath = require('svgpath');

module.exports.scaleIcon = ({ longerAxis }) => {
  return function scaleIcon({ fontAwesomeProps: { width, height, svgPathData, ...props }, ...rest }) {
    const scaledDimensions = scaleDimensionsTo({ longerAxis, width, height });
    const scale = longerAxis / Math.max(width, height);
    const scaledPath = svgPath(svgPathData).scale(scale).toString();
    return { ...rest, fontAwesomeProps: {
      ...props,
      ...scaledDimensions,
      svgPathData: scaledPath
    } };
  };
};

function scaleDimensionsTo({ longerAxis, width, height }) {
  return width < height ?
    { width: Math.round(width * longerAxis / height), height: longerAxis } :
    { height: Math.round(height * longerAxis / width), width: longerAxis };
}
