const { isEmpty, join, pipe, prepend, split, reject } = require('ramda');

module.exports.parseFaProps = function parseFaProps(file) {
  const {
    prefix, nextUnicode,
    name, relativeParent,
    svg: { width, height, path }
  } = file;
  const fontAwesomeProps = {
    prefix: parsePrefix({ root: prefix, relativeParent }),
    iconName: name,
    width: parseInt(width),
    height: parseInt(height),
    ligatures: [],
    unicode: nextUnicode(),
    svgPathData: path,
  };
  return { ...file, rootPrefix: prefix, fontAwesomeProps };
};

function parsePrefix({ root, relativeParent }) {
  return pipe(
      split('/'),
      reject(isEmpty),
      prepend(root),
      join('-')
    )(relativeParent);
}
