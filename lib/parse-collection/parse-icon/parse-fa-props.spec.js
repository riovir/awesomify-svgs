const { always } = require('ramda');

const { parseFaProps } = require('./parse-fa-props');

const parseFaProp = prop => file => parseFaProps(SvgsonFile(file)).fontAwesomeProps[prop];

test('generates Font Awesome prefix from "prefix" option and relativeParent', () => {
  const parseFaPrefix = parseFaProp('prefix');
  expect(parseFaPrefix({ prefix: 'test' })).toBe('test');
  expect(parseFaPrefix({ prefix: 'test', relativeParent: 'subdir' })).toBe('test-subdir');
  expect(parseFaPrefix({ prefix: 'test', relativeParent: 'subdir/deepdir' })).toBe('test-subdir-deepdir');
});

test('returns Font Awesome dimensions as integers', () => {
  const parseFaHeight = parseFaProp('height');
  expect(parseFaHeight({ svg: { height: '42' } })).toBe(42);

  const parseFaWidth = parseFaProp('width');
  expect(parseFaWidth({ svg: { width: '42' } })).toBe(42);
});

test('uses nextUnicode to determine unicode', () => {
  const parseFaUnicode = parseFaProp('unicode');
  expect(parseFaUnicode({ nextUnicode: () => 'f420' })).toBe('f420');
});

function SvgsonFile({
  prefix = 'prefix',
  relativeParent = '',
  nextUnicode = always('f000'),
  svg: {
    width = 32,
    height = 32
  } = {}
} = {}) {
  return { prefix, relativeParent, nextUnicode, svg: { width, height } };
}
