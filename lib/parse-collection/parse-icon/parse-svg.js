const { optimize } = require('svgo');
const { parse: parseContent } = require('svgson');
const { pipeP } = require('../../utils');
const { assocDetails } = require('./assoc-details');
const { svgoConfig } = require('./svgo.config');

module.exports.parseSvg = async function parseSvg({ content, ...rest }) {
  const optimizeSvg = content => optimize(content, svgoConfig);
  return pipeP(
    parseWith({ optimizeSvg, parseContent }),
    assocDetails,
  )({ content, ...rest });
};

function parseWith({ optimizeSvg, parseContent }) {
  return async function parse({ content, ...rest }) {
    const { data } = await optimizeSvg(content);
    const svgsonData = await parseContent(data);
    return { content, ...rest, svgsonData };
  };
}
