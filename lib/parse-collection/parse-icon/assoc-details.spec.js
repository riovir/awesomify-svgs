const { assocDetails } = require('./assoc-details');

test('prefers parsing viewBox for width and height', () => {
  const { svg } = assocDetails(SvgsonData({
    viewBox: '0 0 16 32', width: '55', height: '55'
  }));
  expect(svg).toEqual(expect.objectContaining({ width: '16', height: '32' }));
});

test('returns width and height attributes as fallback', () => {
  const data = SvgsonData({ width: '16', height: '32' });
  delete data.svgsonData.attributes.viewBox;
  const { svg } = assocDetails(data);
  expect(svg).toEqual(expect.objectContaining({ width: '16', height: '32' }));
});

test('throws error when cannot determine dimensions', () => {
  const data = SvgsonData();
  delete data.svgsonData.attributes.viewBox;
  delete data.svgsonData.attributes.width;
  expect(() => assocDetails(data))
      .toThrow(/Unable to determine dimensions/);
});

test('parses path child nodes with name "path"', () => {
  const { svg: { path } } = assocDetails(SvgsonData({
    path: 'some-vector-path'
  }));
  expect(path).toBe('some-vector-path');
});

test('when finding multiple paths appends them and hopes for the best', () => {
  const { svg: { path } } = assocDetails(SvgsonData({
    paths: ['ideally', 'should', 'be', 'only', 'one']
  }));
  expect(path).toBe('ideally should be only one');
});

test('throws error when cannot determine path', () => {
  const data = SvgsonData();
  delete data.svgsonData.children;
  expect(() => assocDetails(data))
      .toThrow(/Unable to determine path/);
});

function SvgsonData({
  viewBox = '0 0 64 48',
  width = '64',
  height = '48',
  path = 'test-path',
  paths = [path]
} = {}) {
  const attributes = { viewBox, height, width };
  const children = paths.map(d => ({ name: 'path', attributes: { d } }));
  const svgsonData = { attributes, children };
  return { svgsonData };
}
