const { assoc } = require('ramda');
const { pipeP } = require('../../utils');
const { parseSvg } = require('./parse-svg');
const { parseFaProps } = require('./parse-fa-props');
const { scaleIcon } = require('./scale-icon');

const FA_WARNING = 'FontAwesome transforms require a viewBox size of 512 to work as expected.';
const AUTO_SCALING_SUGGESTION = 'Either use the --auto-scale flag or ensure SVG viewBox size matches expectations';
const FA_VIEW_BOX_SIZE = 512;

module.exports.parseIcon = ({ prefix, nextUnicode, autoScale }) => {
  const maybeScaleIcon = autoScale ?
      scaleIcon({
        longerAxis: FA_VIEW_BOX_SIZE
      }) :
      verifyViewBox({
        longerAxis: FA_VIEW_BOX_SIZE,
        warn: filePath => `WARNING: ${filePath} has invalid dimensions`,
        explanation: `${FA_WARNING}\n${AUTO_SCALING_SUGGESTION}\n`,
      });
  return function parseIcon(file) {
    return pipeP(
        readContent,
        assoc('prefix', prefix),
        assoc('nextUnicode', nextUnicode),
        parseSvg,
        parseFaProps,
        maybeScaleIcon,
    )(file);
  };
};

async function readContent(file) {
  return { ...file, content: await file.read() };
}

function verifyViewBox({ longerAxis, warn, explanation }) {
  return file => {
    const { relativePath, fontAwesomeProps: { width, height } } = file;
    if (Math.max(width, height) !== longerAxis) {
      console.warn(warn(relativePath));
      console.warn(explanation);
    }
    return file;
  };
}
