const {
  allPass, cond, converge, drop, filter, has, join, map, mergeRight, objOf, path, pick, pipe, prop, propEq, split, tryCatch, T, zipObj
} = require('ramda');

module.exports.assocDetails = function assocDetails({ svgsonData, ...rest }) {
  const parseViewBox = pipe(
    prop('viewBox'),              // take the viewBox property
    split(' '), drop(2),          // cut it up by spaces and ignore the first two
    zipObj(['width', 'height'])   // put the remaining two in an object with props: width and height
  );

  const hasWidthAndHeight = allPass([has('width'), has('height')]);

  const getDimensions = pipe(     // Create a function that pipes a value through these steps
    prop('attributes'),           // Take the attributes property
    cond([                                              // Try parsing it with the following attempts
      [has('viewBox'), parseViewBox],                   // Parse the viewBox if there is one
      [hasWidthAndHeight, pick(['width', 'height'])],   // Pick the width and height properties if both present
      [T, () => { throw new Error(); }]                 // Always throw an error at this point
    ]));

  const getPath = pipe(             // Create a function that pipes a value through these steps
    prop('children'),               // take the children prop, which is an array
    filter(propEq('path', 'name')), // filter elements with name === 'path'
    map(path(['attributes', 'd'])), // convert to array of entry.attributes.d
    join(' '),                      // force paths to a single one resulting in a naive merge
    objOf('path'));                 // wrap it in an object with a 'path' prop

  // Get the value's path and viewBox then merge them to a single object
  const svg = converge(mergeRight, [
    tryCatch(getDimensions, reThrowWith('Unable to determine dimensions. An SVG has to have either a valid viewBox or width and height.')),
    tryCatch(getPath, reThrowWith('Unable to determine path'))
  ])(svgsonData);
  return { svgsonData, ...rest, svg };
};

function reThrowWith(message) {
  return (err, ...args) => {
    throw Object.assign(new Error(message), { cause: err }, { args });
  };
}
