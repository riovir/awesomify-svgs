const { __, ascend, converge, differenceWith, head, indexOf, map, mergeRight, objOf, pipe, prop, reverse, sortWith } = require('ramda');

module.exports.addMissingIcons = function addMissingIcons({ existingIcons, expectedIcons, ...rest }) {
  differenceWith(matchesRelativeName, expectedIcons, existingIcons);
  const findMissingIcons = differenceWith(matchesRelativeName, __, existingIcons);
  const assignClosestMatch = map(closestMatchFrom({ existingIcons, expectedIcons }));
  const missingWithFallback = pipe(findMissingIcons, assignClosestMatch);
  return {
    existingIcons,
    expectedIcons,
    missingIcons: missingWithFallback(expectedIcons),
    ...rest
  };
};

function matchesRelativeName(expected, { relativeName }) {
  return expected === relativeName;
}

function closestMatchFrom({ existingIcons, expectedIcons }) {
  return converge(mergeRight, [
    objOf('relativeName'),
    findFallback({ expectedIcons, existingIcons })
  ]);
}

function findFallback({ existingIcons, expectedIcons }) {
  const rankOf = indexOf(__, reverse(expectedIcons));
  const largestRankDistance = expectedIcons.length - 1;
  const fromHigherRankOnly = missingRank => rank => rank >= missingRank ?
    rank - missingRank :
    largestRankDistance;
  const fromAnyPositiveRank = missingRank => rank => rank >= 0 ? missingRank - rank : largestRankDistance;
  const smallestDistance = smallestDistanceBy(rankOf);
  const bestMatches = missingRank => sortWith([
    smallestDistance(fromHigherRankOnly(missingRank)),
    smallestDistance(fromAnyPositiveRank(missingRank))
  ], existingIcons);
  return pipe(rankOf, bestMatches, head, objOf('fallback'));
}

function smallestDistanceBy(rankOf) {
  return rankDistance => ascend(pipe(prop('relativeName'), rankOf, rankDistance));
}
