const { ifElse, is, map, path, prop, without } = require('ramda');

const { addMissingIcons } = require('./add-missing-icons');

const existingIcons = [
  IconFile('icon-huge'),
  IconFile('icon-large'),
  IconFile('icon-medium'),
  IconFile('icon-small'),
  IconFile('icon-extra')
];
const expectedIcons = ['icon-small', 'icon-medium', 'icon-large', 'icon-huge'];

test('returns existingIcons as is', () => {
  const result = addMissingIcons({ existingIcons, expectedIcons });
  expect(result.existingIcons).toBe(existingIcons);
});

test('returns missingIcons', () => {
  const result = addMissingIcons({ existingIcons, expectedIcons });
  expect(result.missingIcons).toHaveLength(0);
});

testFallbackWhen({ missing: 'icon-large', returns: 'icon-medium' }, 'returns closest existing icon with smaller index');
testFallbackWhen({ missing: 'icon-small', returns: 'icon-medium' }, 'returns closest existing icon otherwise');

testFallbackWhen(
  { only: 'icon-extra', returns: ['icon-extra', 'icon-extra', 'icon-extra', 'icon-extra'] },
  'returns any unexpected icon otherwise');
testFallbackWhen(
  { only: ['icon-huge', 'icon-small'], returns: ['icon-small', 'icon-small'] },
  'favors icon with smaller index over closer one');

function testFallbackWhen({ only, missing, returns }, message) {
  test(message, () => {
    const toFiles = ifElse(is(String), name => [IconFile(name)], map(IconFile));
    const remainingIcons = only ? toFiles(only) : without([IconFile(missing)], existingIcons);
    const result = addMissingIcons({ existingIcons: remainingIcons, expectedIcons });
    const missingNames = map(path(['fallback', 'relativeName']), result.missingIcons);
    const expectedNames = map(prop('relativeName'), toFiles(returns));
    expect(missingNames).toEqual(expectedNames);
  });
}

function IconFile(relativeName) {
  return { relativeName, someData: `data of ${relativeName}` };
}
