const { map } = require('ramda');
const { mapAsync, pipeP } = require('../utils');
const { NextUnicode } = require('./next-unicode');
const { parseIcon } = require('./parse-icon');
const { toIconGroups } = require('./to-icon-groups');
const { addMissingIcons } = require('./add-missing-icons');
const { toTypedIcons } = require('./to-typed-icons');
const { addMainIndices } = require('./add-main-indices');

module.exports.parseCollection = ({ prefix, toBaseName, ensureVariants, namespace, autoScale }) => {
  const nextUnicode = NextUnicode({ from: 'f000' });
  return function parseCollection(files = []) {
    if (!files.length) { return []; }
    return pipeP(
        mapAsync(parseIcon({ prefix, nextUnicode, autoScale })),
        toIconGroups({ toBaseName, ensureVariants }),
        map(addMissingIcons),
        toTypedIcons,
        addMainIndices({ namespace }),
    )(files);
  };
};
