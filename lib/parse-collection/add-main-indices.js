const { map, mapAccum, nth, pipe, reduce, prepend, split, values } = require('ramda');
const { toCamelCase } = require('../utils');
const { IndexFile } = require('../outputs');

module.exports.addMainIndices = ({ namespace }) => {
  return function addMainIndices(typedIcons) {
    const indexFiles = pipe(
      reduce(byGroupChildren, {}),
      map(allIcons => IndexFile({ allIcons })),
      map(({ prefix, ...rest }) => IndexFile({
        prefix, ...rest,
        namespace: toCamelCase(`${namespace}-${prefix}`)
      })),
      values,
    )(typedIcons);
    return [...typedIcons, ...indexFiles];
  };
};

function byGroupChildren(groups, icon) {
  relativeParents(icon).forEach(parent => {
    const group = groups[parent] || [];
    groups[parent] = [...group, icon];
  });
  return groups;
}

function relativeParents({ relativeParent }) {
  if (!relativeParent) { return ['']; }
  const asPath = (root, dir) => !root ? dir : `${root}/${dir}`;
  return pipe(
    split('/'),
    mapAccum((root, dir) => [
      asPath(root, dir),
      asPath(root, dir)
    ], ''),
    nth(1),
    prepend('')
  )(relativeParent);
}
