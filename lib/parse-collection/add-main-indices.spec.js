const { propEq } = require('ramda');
const { ExistingIcon } = require('../outputs');

const { addMainIndices } = require('./add-main-indices');

const addTestIndices = addMainIndices({ namespace: 'test' });

test('does not add anything to empty array', () => {
  expect(addTestIndices([])).toEqual([]);
});

test('appends index to outputs end', () => {
  const [one, two, index, ...rest] = addTestIndices([
    TestIcon({ name: 'one' }),
    TestIcon({ name: 'two' }),
  ]);
  expect(rest).toHaveLength(0);
  expect(index.name).toEqual('index');
  expect(index.allIcons).toEqual([one, two]);
});

test('append an index for each relativeParent each containing children recursively', () => {
  const [one, two, three, ...indices] = addTestIndices([
    TestIcon({ name: 'one', relativeParent: '' }),
    TestIcon({ name: 'two', relativeParent: 'dir' }),
    TestIcon({ name: 'three', relativeParent: 'dir/deep' }),
  ]);
  expect(indices).toHaveLength(3);
  const rootIndex = indices.find(propEq('index', 'relativeName'));
  const subIndex = indices.find(propEq('dir/index', 'relativeName'));
  const deepIndex = indices.find(propEq('dir/deep/index', 'relativeName'));
  expect(rootIndex.allIcons).toEqual([one, two, three]);
  expect(subIndex.allIcons).toEqual([two, three]);
  expect(deepIndex.allIcons).toEqual([three]);
});

function TestIcon({
  name = 'test-icon',
  relativeParent = '',
  relativeName = relativeParent ? `${relativeParent}/${name}` : name,
  data: {
    prefix = 'test-prefix',
    ...rest
  } = {}
} = {}) {
  return ExistingIcon({ relativeParent, relativeName, name, data: { prefix, ...rest } });
}
