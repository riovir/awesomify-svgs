const { always, groupBy, mapObjIndexed, pipe, prop, values } = require('ramda');

module.exports.toIconGroups = function toIconGroups({
  toBaseName = n => n,
  ensureVariants = always([])
} = {}) {
  const byRelativeName = pipe(prop('relativeName'), toBaseName);
  const toCollections = (files, baseName) => {
    const expectedIcons = ensureVariants(baseName);
    return { name: baseName, existingIcons: files, expectedIcons };
  };
  return pipe(
    groupBy(byRelativeName),
    mapObjIndexed(toCollections),
    values);
};

