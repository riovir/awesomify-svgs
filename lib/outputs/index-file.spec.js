const { ExistingIcon } = require('./existing-icon');
const { IndexFile } = require('./index-file');

test('defaults allIcons to empty array', () => {
  expect(IndexFile({}).allIcons).toEqual([]);
});

test('favors props that are set', () => {
  expect(IndexFile({
    relativeParent: 'test-parent',
    relativeName: 'test-name',
    name: 'name'
  })).toEqual(expect.objectContaining({
    relativeParent: 'test-parent',
    relativeName: 'test-name',
    name: 'name'
  }));
});

test('guesses relativeParent from allIcons root parent', () => {
  const { relativeParent } = IndexFile({ allIcons: [
    TestIcon({ relativeParent: 'dir' }),
    TestIcon({ relativeParent: 'dir/deep' }),
  ] });
  expect(relativeParent).toBe('dir');
});

test('guesses relativeName from allIcons', () => {
  const relativeNameHaving = allIcons => IndexFile({ allIcons }).relativeName;

  expect(relativeNameHaving([
    TestIcon({ relativeParent: '' }),
    TestIcon({ relativeParent: 'dir' }),
    TestIcon({ relativeParent: 'dir/deep' }),
  ])).toBe('index');

  expect(relativeNameHaving([
    TestIcon({ relativeParent: 'dir' }),
    TestIcon({ relativeParent: 'dir/deep' }),
  ])).toBe('dir/index');
});

function TestIcon({
  name = 'test-icon',
  relativeParent = '',
  relativeName = relativeParent ? `${relativeParent}/${name}` : name,
  data: {
    prefix = 'test-prefix',
    ...rest
  } = {}
} = {}) {
  return ExistingIcon({ relativeParent, relativeName, name, data: { prefix, ...rest } });
}
