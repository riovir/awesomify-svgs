const { missing: type } = require('./type');

module.exports.MissingIcon = function MissingIcon({ relativeName, name = guessNameFrom(relativeName), fallback }) {
  return {
    name,
    relativeName,
    fallback,
    relativeParent: fallback.relativeParent,
    prefix: fallback.prefix,
    type
  };
};

function guessNameFrom(relativeName) {
  const [last] = relativeName.split('/').reverse();
  return last;
}
