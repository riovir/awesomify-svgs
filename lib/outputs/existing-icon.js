const { existing: type } = require('./type');

module.exports.ExistingIcon = function ExistingIcon({ relativeParent, relativeName, name, data }) {
  return { relativeParent, relativeName, name, data, prefix: data.prefix, type };
};
