const { ascend, groupBy, head, keys, pipe, prop, propEq, sortWith, splitAt } = require('ramda');
const { index: type } = require('./type');

module.exports.IndexFile = function IndexFile({
  namespace,
  allIcons = [],
  relativeParent = shortestProp('relativeParent', allIcons),
  relativeName = guessRelativeNameFrom(allIcons),
  name = guessNameFrom({ relativeParent, relativeName }),
}) {
  const prefix = shortestProp('prefix', allIcons);
  const directIcons = allIcons.filter(propEq(prefix, 'prefix'));
  const iconsByPrefix = groupByPrefix({ prefix, allIcons });

  return { namespace, allIcons, directIcons, iconsByPrefix, relativeParent, relativeName, name, prefix, type };
};

function groupByPrefix({ allIcons }) {
  const renameKeysToPrefix = grouping => Object.values(grouping)
      .reduce((group, icons) => ({ ...group, [shortestProp('prefix', icons)]: icons }), {});
  return pipe(
    groupBy(prop('relativeParent')),
    includeDeepChildren,
    renameKeysToPrefix
  )(allIcons);
}

function includeDeepChildren(groupedIcons) {
  const childrenOf = dir => Object.entries(groupedIcons)
      .reduce((icons, [parentDir, children]) => parentDir.startsWith(dir) ? icons.concat(children) : icons, []);
  return Object.keys(groupedIcons)
      .reduce((grouping, parent) => ({ ...grouping, [parent]: childrenOf(parent) }), {});
}

function shortestProp(propName, array) {
  if (!array.length) { return; }
  return pipe(
    groupBy(prop(propName)),
    keys,
    sortWith([ascend(prop('length'))]),
    head
  )(array);
}

function guessRelativeNameFrom(icons) {
  const relativeParent = shortestProp('relativeParent', icons);
  if (relativeParent === '') { return 'index'; }
  if (relativeParent === undefined) { return; }
  return `${relativeParent}/index`;
}

function guessNameFrom({ relativeParent, relativeName }) {
  if (!relativeParent) { return relativeName; }
  return splitAt(relativeParent.length + 1, relativeName)[1];
}
