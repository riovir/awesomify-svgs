module.exports = Object.freeze({
  existing: 'existing',
  missing: 'missing',
  index: 'index'
});
