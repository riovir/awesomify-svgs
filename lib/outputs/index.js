module.exports = {
  outputType: require('./type'),
  ...require('./existing-icon'),
  ...require('./missing-icon'),
  ...require('./index-file'),
};
