/** Converts SVG icons in a dir to a FontAwesome compliant icon pack. */
export function awesomifySvgs(options?: AwesomifyOptions): Promise<Array<Promise<void>>>;

type AwesomifyOptions = {
  /** Relative or absolute path to SVGs to convert. Defaults to "icons". */
  source?: string,
  /** Relative or absolute path to target directory. Defaults to "dist". */
  target?: string,
  /** Output FontAwesome format to use. Defaults to "latest". */
  format?: FontAwesomeFormat,
  /** Prepending the prefix, used as a namespace for UMD bundled index.js files. Defaults to "Awesomify". */
  namespace?: string,
  /** Opts into auto-scaling viewBox and path coordinates to 512. Defaults to false. */
  autoScale?: boolean,
  /** Deep scan (recursively) the source directory for icons. Defaults to true. */
  deep?: boolean,
  /** Wipe target first. Defaults to false. */
  cleanTarget?: boolean,
  /** Set the prefix property on the icon entries. Any subdirectories will be appended to this inside the source. Defaults to "the". */
  prefix?: string,
  /** ADVANCED: Group icon variants together. Eg. to recognize user-small and user-large both as user, chop off the last bit. */
  toBaseName?: (relativeName: string) => string,
  /** ADVANCED: Generate fallbacks for missing icons. Eg. to ensure user-small, user-medium, and user-large to exist, return them in an array */
  ensureVariants?: (baseName: string) => Array<string>
};

type FontAwesomeFormat =
  /** Latest supported FontAwesome format */
  'latest' |
  /** Adds an tree-shakeable ESM based format (index.es.js) introduced in FontAwesome 5.1 */
  'v5-esm' |
  /** CommonJS based format in FontAwesome 5.0 */
  'v5-cjs';
