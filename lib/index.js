const { chewDir } = require('chew-dir');
const { pipeP } = require('./utils');
const { parseCollection } = require('./parse-collection');
const { generateOutput } = require('./generate-output');
const { v5cjs, v5esm } = require('./formatters');

/** Formatters for various Font Awesome output formats */
const FA_FORMATTERS = {
  'latest': v5esm,
  'v5-esm': v5esm,
  'v5-cjs': v5cjs,
};

module.exports.awesomifySvgs = async function awesomifySvgs({
  source = 'icons',
  target = 'dist',
  format = 'latest',
  namespace = 'Awesomify',
  autoScale = false,
  deep = true,
  cleanTarget = false,
  prefix = 'the',
  toBaseName = relativeName => relativeName,
  ensureVariants = _baseName => [],
} = {}) {
  const formatter = FA_FORMATTERS[format];
  if (!formatter) {
    throw new Error(`Format ${format} not recognized. Accepted values: [${Object.keys(FA_FORMATTERS)}]`);
  }
  return chewDir({
    source,
    target,
    cleanTarget,
    deep,
    include: /\.svg$/i,
    processAll: (files, api) => pipeP(
        parseCollection({ prefix, toBaseName, ensureVariants, namespace, autoScale }),
        generateOutput({ formatter }),
        writeFilesWith({ api })
    )(files)
  });
};

function writeFilesWith({ api }) {
  return files => files.map(({ relativeName, content }) => api.outputFile(relativeName, content));
}
