const { flatten } = require('ramda');
const { outputType } = require('./outputs');

module.exports.generateOutput = ({ formatter }) => {
  return async function generateOutput(typedAssets) {
    const fileOutputs = await Promise.all(typedAssets.map(({ type, ...rest }) => {
      if (type === outputType.existing) { return formatter.existingIconOutput({ type, ...rest }); }
      if (type === outputType.missing) { return formatter.missingIconOutput({ type, ...rest }); }
      if (type === outputType.index) { return formatter.indexFileOutput({ type, ...rest }); }
      return [];
    }));
    return Promise.all(flatten(fileOutputs));
  };
};
