const { toCamelCase } = require('./to-camel-case');

test('regards argument as String', () => {
  expect(toCamelCase(2)).toBe('2');
});

test('considers common separators', () => {
  expect(toCamelCase('camel-case')).toBe('camelCase');
  expect(toCamelCase('camel case')).toBe('camelCase');
  expect(toCamelCase('camel_case')).toBe('camelCase');
  expect(toCamelCase('camel/case')).toBe('camelCase');
});

test('returns string without separator as is', () => {
  expect(toCamelCase('')).toBe('');
  expect(toCamelCase('camel')).toBe('camel');
});

test('returns blank values as empty string', () => {
  expect(toCamelCase()).toBe('');
  expect(toCamelCase(null)).toBe('');
  expect(toCamelCase(false)).toBe('false');
});
