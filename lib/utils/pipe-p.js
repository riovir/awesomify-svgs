const { andThen, pipeWith } = require('ramda');

module.exports.pipeP = function pipeP(...args) {
  return pipeWith(andThen)(args);
};
