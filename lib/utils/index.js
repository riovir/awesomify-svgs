module.exports = {
  ...require('./to-camel-case'),
  ...require('./map-async'),
  ...require('./pipe-p'),
};
