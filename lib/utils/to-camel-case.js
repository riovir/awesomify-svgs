const { splitAt } = require('ramda');

module.exports.toCamelCase = function toCamelCase(string = '') {
  if (string === null) { return ''; }
  const capitalizeFirst = string => {
    const [first, rest] = splitAt(1, string);
    return `${first.toUpperCase()}${rest}`;
  };
  const [first, ...rest] = String(string).split(/[-_ /]/);
  return [first, ...rest.map(capitalizeFirst)].join('');
};
