const { curry, map } = require('ramda');

module.exports.mapAsync = curry(function mapAsync(mapper, values) {
  return Promise.all(map(mapper, values));
});
