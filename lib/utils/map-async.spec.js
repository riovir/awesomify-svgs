const { mapAsync } = require('./map-async');

test('returns function on only mapper passed in', () => {
  const mapper = i => i * 2;
  return expect(mapAsync(mapper)).toBeInstanceOf(Function);
});

test('returns Promise on both arguments passed in', () => {
  const mapper = i => i * 2;
  return expect(mapAsync(mapper, [])).toBeInstanceOf(Promise);
});

test('uses mapper on array', () => {
  const mapper = i => i * 2;
  return expect(mapAsync(mapper, [1, 2, 3])).resolves.toEqual([2, 4, 6]);
});

test('can be curried', async () => {
  const mapper = i => i * 2;
  const array = [1, 2, 3];
  const normalResult = await mapAsync(mapper, array);
  const curriedResult = await mapAsync(mapper)(array);
  return expect(normalResult).toEqual(curriedResult);
});
